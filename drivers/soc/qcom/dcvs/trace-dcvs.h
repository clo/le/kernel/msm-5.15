/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2020, The Linux Foundation. All rights reserved.
 */

#if !defined(_TRACE_DCVS_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_DCVS_H

#undef TRACE_SYSTEM
#define TRACE_SYSTEM dcvs

#include <linux/tracepoint.h>
#include <soc/qcom/dcvs.h>

#endif /* _TRACE_DCVS_H */

#undef TRACE_INCLUDE_PATH
#define TRACE_INCLUDE_PATH .

#undef TRACE_INCLUDE_FILE
#define TRACE_INCLUDE_FILE trace-dcvs

#include <trace/define_trace.h>
