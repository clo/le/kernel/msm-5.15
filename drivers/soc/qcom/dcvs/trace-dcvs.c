// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (c) 2020, The Linux Foundation. All rights reserved.
 */

#include <soc/qcom/dcvs.h>

#define CREATE_TRACE_POINTS
#include "trace-dcvs.h"

